from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse
from django.db import connection
import os
import json

# Create your views here.
response = {}
def index(request):
    return render(request, 'login.html', response)

def error(request):
    return render(request, 'log_in_fail.html', response)

def loginpage(request):
    if request.method == 'POST':
        c = connection.cursor()

        username = request.POST['username']
        password = request.POST['password']

        c.execute('SELECT p.username FROM SIMUI.pengguna p join admin a on a.username = p.username;')
        daftarAdmin =  c.fetchall()

        c.execute('SELECT p.username FROM SIMUI.pengguna p join non_admin a on a.username = p.username;')
        daftarCivitas =  c.fetchall()

        c.execute('SELECT p.username FROM SIMUI.pengguna p join guest a on a.username = p.username;')
        daftarHumas =  c.fetchall()

        isAdmin = False
        isHumas = False
        isCivitas = False

        for uname in daftarAdmin :
            if (uname[0] == username):
                isAdmin= True
        for uname in daftarCivitas :
            if (uname[0] == username):
                isCivitas= True
        for uname in daftarHumas :
            if (uname[0] == username):
                isHumas = True

        passwordUser = ""

        if (isAdmin or isHumas or isCivitas) :
            c.execute("SELECT password FROM SIMUI.pengguna WHERE username = '" + username + "'")
            data =  c.fetchone()
            passwordUser = data[0]

        else :
            response['msg'] = "username salah"
            c.close()
            return redirect(reverse('errorpage'))

        if (password==passwordUser) :
            request.session['username'] = username

            if(isAdmin) :
                request.session['role'] = "admin"
            elif(isHumas) :
                request.session['role'] = "humas"
            elif(isCivitas) :
                request.session['role'] = "civitas"


            request.session['is_login'] = "login"

            if (isAdmin):
                return redirect(reverse('admin_page:index'))

            elif (isHumas):
                return redirect(reverse('humas_page:index'))

            elif (isCivitas):
                return redirect(reverse('civitas_page:index'))

            elif (fail) :
                return redirect(reverse('login:error'))

        else :
            response['msg'] = "password salah"
            c.close()
            return redirect(reverse('errorpage'))



            # if (notExist) :
            #     return redirect(reverse('login:error'))


def logout(request):
    try:
        request.session.flush()
    except KeyError:
        pass
    return redirect(reverse('landingpage:index'))
