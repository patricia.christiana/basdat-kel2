from django.conf.urls import url
from .views import index, loginpage,logout,error
from django.contrib.auth import views as auth_views


# url for app, add your URL Configuration
urlpatterns = [
	url(r'^$', index, name='index'),
    url(r'^signin/$', loginpage, name='loginpage'),
	url(r'^signout/$', logout, name ='logoutpage'),
	url(r'^error/$', error, name ='errorpage'),

    ]
