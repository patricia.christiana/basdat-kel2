from django.apps import AppConfig


class DaftarEventConfig(AppConfig):
    name = 'daftar_event'
