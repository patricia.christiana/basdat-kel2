"""TKBasdat URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import RedirectView
import landingpage.urls as index
import loginpage.urls as login
# import admin_page.urls as admin
import humas_page.urls as humas
import civitas_page.urls as guest


urlpatterns = [
    url(r'^admin/', admin.site.urls),
	url(r'^landing/',include('landingpage.urls')),
    url(r'^login/', include('loginpage.urls')),
	url(r'^admin_page/', include('admin_page.urls')),
    url(r'^humas_page/', include('humas_page.urls')),
    url(r'^civitas_page/', include('civitas_page.urls')),
    # url(r'^$', RedirectView.as_view(url='/index/', permanent = True), name='$'),
	# url(r'^landing/',include(index , namespace ='landingpage')),
    # url(r'^login/', include(login, namespace = 'loginpage')),
	# url(r'^admin_page/', include(admin, namespace = 'admin_page')),
    # url(r'^humas_page/', include(humas, namespace ='humas_page')),
    # url(r'^civitas_page/', include(guest, namespace='civitas_page')),
    url(r'^$', RedirectView.as_view(url='/index/', permanent = True), name='$'),

    ]
