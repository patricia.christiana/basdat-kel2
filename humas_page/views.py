from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.db import connection
from django.urls import reverse

# Create your views here.
def humas(request):
    response = {}
    return render(request, 'humas_page.html', response)
