from django.apps import AppConfig


class HumasPageConfig(AppConfig):
    name = 'humas_page'
