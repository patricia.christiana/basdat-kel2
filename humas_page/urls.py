from django.conf.urls import url
from .views import humas
from django.contrib.auth import views as auth_views

app_name = 'humas_page'
# url for app, add your URL Configuration
urlpatterns = [
    url(r'^humas/$', humas, name='index')

    ]
