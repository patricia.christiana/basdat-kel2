from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.db import connection
from django.urls import reverse

# Create your views here.
def index(request):
    cursor = connection.cursor()
    response = {}

    cursor.execute("SELECT DISTINCT nama FROM SIMUI.pembuat_event JOIN organisasi  on id_organisasi = id limit 5; ")
    response['organisasi'] = cursor.fetchall()

    cursor.execute("SELECT DISTINCT nama FROM SIMUI.pembuat_event JOIN kepanitiaan  on id_kepanitiaan = id limit 5; ")
    response['kepanitiaan'] = cursor.fetchall()

    cursor.execute("SELECT DISTINCT nama FROM SIMUI.event limit 5; ")
    response['event'] = cursor.fetchall()

    return render(request, 'landingpage.html', response)



# def loginpage(request):
#     response = {}
#     return render(request, 'login.html', response)
