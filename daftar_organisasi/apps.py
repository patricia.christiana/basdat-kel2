from django.apps import AppConfig


class DaftarOrganisasiConfig(AppConfig):
    name = 'daftar_organisasi'
