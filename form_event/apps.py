from django.apps import AppConfig


class FormEventConfig(AppConfig):
    name = 'form_event'
