from django.apps import AppConfig


class DaftarKepanitiaanConfig(AppConfig):
    name = 'daftar_kepanitiaan'
