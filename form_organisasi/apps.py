from django.apps import AppConfig


class FormOrganisasiConfig(AppConfig):
    name = 'form_organisasi'
