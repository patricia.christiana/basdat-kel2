from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.db import connection
from django.urls import reverse

# Create your views here.
def admin(request):
    response = {}
    return render(request, 'admin_page.html', response)
