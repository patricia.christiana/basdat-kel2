from django.conf.urls import url
from .views import admin
from django.contrib.auth import views as auth_views

app_name = 'admin_page'
# url for app, add your URL Configuration
urlpatterns = [
    url(r'^admin/$', admin, name='index')

    ]
