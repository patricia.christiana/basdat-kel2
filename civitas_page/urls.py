from django.conf.urls import url
from .views import guest
from django.contrib.auth import views as auth_views

app_name = 'civitas_page'
# url for app, add your URL Configuration
urlpatterns = [
    url(r'^guest/$', guest, name='index')

    ]
