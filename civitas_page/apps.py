from django.apps import AppConfig


class CivitasPageConfig(AppConfig):
    name = 'civitas_page'
